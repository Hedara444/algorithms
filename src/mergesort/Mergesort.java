/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mergesort;

import java.util.Scanner;


public class Mergesort {
    public static void merge(int low, int mid, int high, int[] t) {
        int i, j, k;
        int[] aux = new int[8];

        i = low;
        k = low; // both k and i setting on the low value
        j = mid + 1; // counter for the second array
        // while loop to iterate through all the values of the 2 sub-arrays
        while ((i <= mid) && (j <= high)) {
            // if the value of the first element is less than the value of the second array element
            if (t[i] < t[j]) {
                aux[k] = t[i]; // add the value of the first
                k++; // mov the k to the  next  index
                i++; // mov i to the next element in the first array
            } else if (t[i] > t[j]) {
                aux[k] = t[j]; // add the value of the second element
                k++;  // mov k to the next index
                j++; // mov j to the next  element in the second array
            } else    // t[i] == t[j]
            {
                aux[k] = t[i]; // add the value of the first element
                aux[k + 1] = t[j]; // add the value of the second element inn the next index in the merge array
                k = k + 2; // mov k by 2 ; because we have added two element on sconce
                i++;
                j++;
            }
        }
        // after sorting the most of the elements we will have some elements  need to be added to the merge array
        while (i <= mid) {
            aux[k] = t[i];
            k++;
            i++;
        }
        while (j <= high) {
            aux[k] = t[j];
            k++;
            j++;
        }

        i = low; // mov i to the index of the first array
        // add all the values of the merge array to the final array
        while (i <= high) {
            t[i] = aux[i];
            i++;
        }
    }

    public static void mergeSort(int low, int high, int[] t) {
        int mid; // the middle
        // if we didn't yet reach the end of the array
        if (low != high) {
            mid = (low + high) / 2;
            mergeSort(low, mid, t); // cal the merge sort again for the new array  (low-------> mid)
            mergeSort(mid + 1, high, t); //call the merge sort again for the new array (mid+1 ------> high)
            merge(low, mid, high, t); // call the merge function for the all 3 params (low ---> mid, mid+1 --- high)
        }
    }


    public static void main(String[] args) {

        int i, x, n;
        int[] tv = new int[8];
        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        for (i = 0; i < n; i++) {
            x = input.nextInt();
            //x = Integer.parseInt(JOptionPane.showInputDialog("Enter Number:"));
            tv[i] = x;
        }

        mergeSort(0, n - 1, tv);

        System.out.println("after Sort");
        for (i = 0; i < n; i++) {
            System.out.print(tv[i] + "\t");
        }
        System.out.println();
    }
}
